ARG VERSION=latest

FROM wordpress:${VERSION}

COPY install.sh /root/install.sh

RUN /bin/bash /root/install.sh

ENTRYPOINT [ "/root/entrypoint.sh" ]

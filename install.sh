#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	apt-transport-https \
	ca-certificates \
	curl \
	dnsutils \
	git \
	libcurl4 \
	libxslt1.1 \
	libzip4 \
	libcurl4-openssl-dev \
	libfreetype6 \
	libfreetype6-dev \
	libicu-dev \
	libonig-dev \
    libxslt1-dev \
	procps \
	rsync \
	wget \
	unzip \
	zip
# Add php extensions
docker-php-ext-install -j$(nproc) intl mbstring pdo pdo_mysql soap xsl
pecl install redis xdebug
docker-php-ext-enable redis
pecl clear-cache
# Configure xdebug but disable it
cat <<EOF > /usr/local/etc/php/conf.d/xdebug.ini
[xdebug]
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_port=9001
xdebug.auto_trace=0
xdebug.trace_enable_trigger=1
xdebug.trace_output_dir=/tmp
xdebug.trace_output_name=trace.%H.%R.%u
xdebug.profiler_enable=0
xdebug.profiler_enable_trigger=1
xdebug.profiler_output_dir=/tmp
xdebug.profiler_output_name=cachegrind.%H.%R.%u
EOF
# Mcrypt
if php -r 'exit(PHP_VERSION_ID < 70200 ? 0 : 1);'
then
	apt-get --yes --quiet=2 install libmcrypt4 libmcrypt-dev
	docker-php-ext-install -j$(nproc) mcrypt
	apt-get remove -y --auto-remove libmcrypt-dev
fi
# Purge unnecessary files
apt-get remove -y --auto-remove libcurl4-openssl-dev libfreetype6-dev libicu-dev libxslt1-dev libonig-dev
apt-get clean
rm -rf /var/lib/apt/lists/*
# Add composer to path
cat <<EOF > /etc/profile.d/composer.sh
#!/bin/sh
export GLOBAL_COMPOSER_HOME="/usr/local/share/composer"
export PATH="\${GLOBAL_COMPOSER_HOME}/vendor/bin:\${PATH}"
export COMPOSER_ALLOW_SUPERUSER=1
export COMPOSER_NO_INTERACTION=1
EOF
source /etc/profile.d/composer.sh
mkdir -p /usr/local/share/composer
ln -s /usr/local/share/composer /root/.composer
# Fecth composer and install at /usr/local/bin/composer
mkdir -p /usr/local/bin
cd /usr/local/bin
curl -sS https://getcomposer.org/installer | php
chmod +x /usr/local/bin/composer.phar
mv /usr/local/bin/composer.phar /usr/local/bin/composer
# Install parallel downloads for composer
composer global require hirak/prestissimo --prefer-dist --no-interaction --no-progress
# Change default shell for web user
usermod -s /bin/bash www-data
# Remove previous Wordpress install
rm -Rf /var/www/html/*
# Entrypoint with correct rights
cat <<EOF > /root/entrypoint.sh
#!/bin/bash
if [ -n "\${HOST_USER_ID}" ]
then
	usermod -o -u "\${HOST_USER_ID}" www-data
	groupmod -o -g "\${HOST_GROUP_ID}" www-data
fi
if [ -n "\${DEBUG}" ] && [ "\${DEBUG}" == "1" ]
then
	docker-php-ext-enable xdebug
fi
su - www-data -m -c "cd /var/www/html && bin/install.sh"
php-fpm
EOF
chmod +x /root/entrypoint.sh
